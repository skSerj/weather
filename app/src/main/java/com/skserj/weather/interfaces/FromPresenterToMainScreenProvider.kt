package com.skserj.weather.interfaces

import com.skserj.weather.model.network.WeatherInfo

interface FromPresenterToMainScreenProvider {
    fun updateInfo(it: List<WeatherInfo>)
}
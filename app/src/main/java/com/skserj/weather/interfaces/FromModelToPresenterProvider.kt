package com.skserj.weather.interfaces

import com.skserj.weather.model.network.WeatherInfo

interface FromModelToPresenterProvider {
    fun getWeatherData(it: List<WeatherInfo>)
}
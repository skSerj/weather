package com.skserj.weather

import android.app.Application
import com.skserj.weather.model.db.WeatherDataBase

class App :Application() {
    lateinit var db: WeatherDataBase
    override fun onCreate() {
        super.onCreate()
        db = WeatherDataBase.getInstance(this)
    }
}
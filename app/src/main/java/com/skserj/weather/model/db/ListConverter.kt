package com.skserj.weather.model.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.skserj.weather.model.network.WeatherInfo

class ListConverter {

    private val gson = Gson()
    private val type = object : TypeToken<List<WeatherInfo>>() {}.type

    @TypeConverter
    fun toProductionCountry(countries: List<WeatherInfo>?): String? =
        gson.toJson(countries)

    @TypeConverter
    fun fromProductionCountry(data: String): List<WeatherInfo>? =
        gson.fromJson(data, type)
}

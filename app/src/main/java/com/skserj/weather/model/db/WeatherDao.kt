package com.skserj.weather.model.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.skserj.weather.model.network.RequestModel

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWeatherData (weatherDB: RequestModel)

    @Query("SELECT * FROM RequestModel")
    fun selectAll(): RequestModel

}
package com.skserj.weather.model

import android.content.Context
import android.util.Log
import com.skserj.weather.App
import com.skserj.weather.interfaces.FromModelToPresenterProvider
import com.skserj.weather.model.network.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainModel(
    private val fromModelToPresenterProvider: FromModelToPresenterProvider,
    context: Context
) {
    private lateinit var disposable: Disposable

    private val dao = (context.applicationContext as App).db.getWeatherDao()

    fun loadDataFromServer(weatherType: String) {
        disposable = ApiService.getFiveDayWeather()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("MyApp", "response ok")
                dao.insertWeatherData(it)
                if (weatherType.contains("по дням") || weatherType.contains("by days")) {
                    fromModelToPresenterProvider.getWeatherData(it.list.filter {
                        it.dtTxt!!.contains(
                            "12:00:00"
                        )
                    })
                } else {
                    fromModelToPresenterProvider.getWeatherData(it.list)
                }
            }, {
//                fromModelToPresenterProvider.getWeatherData(dao.selectAll().list)
                Log.d("MyApp", " response error")
                it.printStackTrace()
            })
    }
}
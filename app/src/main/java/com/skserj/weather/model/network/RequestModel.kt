package com.skserj.weather.model.network

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.skserj.weather.model.db.ListConverter

@Entity
data class RequestModel(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @SerializedName("list")
    @TypeConverters(ListConverter::class)
    var list: List<WeatherInfo>
)

data class WeatherInfo(
    var main: Main?,
    var weather: List<Weather>?,
    var clouds: Clouds?,
    var wind: Wind?,
    @SerializedName("dt_txt")
    var dtTxt: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Main::class.java.classLoader),
        parcel.createTypedArrayList(Weather),
        parcel.readParcelable(Clouds::class.java.classLoader),
        parcel.readParcelable(Wind::class.java.classLoader),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(main, flags)
        parcel.writeTypedList(weather)
        parcel.writeParcelable(clouds, flags)
        parcel.writeParcelable(wind, flags)
        parcel.writeString(dtTxt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WeatherInfo> {
        override fun createFromParcel(parcel: Parcel): WeatherInfo {
            return WeatherInfo(parcel)
        }

        override fun newArray(size: Int): Array<WeatherInfo?> {
            return arrayOfNulls(size)
        }
    }
}

data class Main(
    var temp: Float,
    @SerializedName("feels_like")
    var feelsLike: Float,
    @SerializedName("temp_min")
    var tempMin: Float,
    @SerializedName("temp_max")
    var tempMax: Float,
    var pressure: Int,
    var humidity: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(temp)
        parcel.writeFloat(feelsLike)
        parcel.writeFloat(tempMin)
        parcel.writeFloat(tempMax)
        parcel.writeInt(pressure)
        parcel.writeInt(humidity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Main> {
        override fun createFromParcel(parcel: Parcel): Main {
            return Main(parcel)
        }

        override fun newArray(size: Int): Array<Main?> {
            return arrayOfNulls(size)
        }
    }
}

data class Wind(
    var speed: Float,
    var deg: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readFloat(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(speed)
        parcel.writeInt(deg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Wind> {
        override fun createFromParcel(parcel: Parcel): Wind {
            return Wind(parcel)
        }

        override fun newArray(size: Int): Array<Wind?> {
            return arrayOfNulls(size)
        }
    }
}

data class Clouds(var all: Int):Parcelable {
    constructor(parcel: Parcel) : this(parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(all)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Clouds> {
        override fun createFromParcel(parcel: Parcel): Clouds {
            return Clouds(parcel)
        }

        override fun newArray(size: Int): Array<Clouds?> {
            return arrayOfNulls(size)
        }
    }
}

data class Weather(
    var id: Int,
    var main: String?,
    var description: String?,
    var icon: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(main)
        parcel.writeString(description)
        parcel.writeString(icon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Weather> {
        override fun createFromParcel(parcel: Parcel): Weather {
            return Weather(parcel)
        }

        override fun newArray(size: Int): Array<Weather?> {
            return arrayOfNulls(size)
        }
    }
}




package com.skserj.weather.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.skserj.weather.model.network.RequestModel

@Database(entities = [RequestModel::class], version = 1, exportSchema = false)
@TypeConverters(ListConverter::class)
abstract class WeatherDataBase : RoomDatabase() {
    abstract fun getWeatherDao(): WeatherDao

    companion object {
        fun getInstance(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            WeatherDataBase::class.java,
            "weatherDB"
        ).fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()
    }
}
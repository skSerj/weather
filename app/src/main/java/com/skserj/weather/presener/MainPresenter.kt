package com.skserj.weather.presener

import android.content.Context
import com.skserj.weather.interfaces.FromModelToPresenterProvider
import com.skserj.weather.interfaces.FromPresenterToMainScreenProvider
import com.skserj.weather.model.MainModel
import com.skserj.weather.model.network.WeatherInfo

class MainPresenter(
    private var fromPresenterToMainScreenProvider: FromPresenterToMainScreenProvider,
    val context: Context
) :
    FromModelToPresenterProvider {

    private lateinit var mainModel: MainModel

    fun takeDataFromModel(weatherType: String) {
        mainModel = MainModel(this, context)
        mainModel.loadDataFromServer(weatherType)
    }

    override fun getWeatherData(it: List<WeatherInfo>) {
        fromPresenterToMainScreenProvider.updateInfo(it)
    }


}
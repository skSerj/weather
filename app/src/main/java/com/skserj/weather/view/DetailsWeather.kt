package com.skserj.weather.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.skserj.weather.R
import com.skserj.weather.model.network.WeatherInfo
import kotlinx.android.synthetic.main.fragment_details_weather.*

class DetailsWeather : Fragment() {

    lateinit var weatherModel: WeatherInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        weatherModel = requireArguments().getParcelable("weatherData")!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details_weather, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detail_day_info.text = weatherModel.dtTxt
        detail_weather_info.text = weatherModel.weather?.get(0)?.description
        detail_weather_temp.text = weatherModel.main?.temp.toString() + " ℃"
        min_weather_temp.text = weatherModel.main?.tempMin.toString() + " ℃"
        max_weather_temp.text = weatherModel.main?.tempMax.toString() + " ℃"
        feel_like_weather_temp.text = weatherModel.main?.feelsLike.toString() + " ℃"
        wind_speed.text = weatherModel.wind?.speed.toString()
        pressure.text = weatherModel.main?.pressure.toString()
        humidity.text = weatherModel.main?.humidity.toString()
        Glide.with(view.context)
            .load("http://openweathermap.org/img/wn/${weatherModel.weather?.get(0)?.icon}.png")
            .into(detail_weather_image)
    }
}
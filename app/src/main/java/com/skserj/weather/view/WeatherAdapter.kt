package com.skserj.weather.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.skserj.weather.R
import com.skserj.weather.model.network.WeatherInfo
import kotlinx.android.synthetic.main.item_main_screen.view.*

class WeatherAdapter() : RecyclerView.Adapter<WeatherAdapter.WeatherHolder>() {

    private val weatherList = ArrayList<WeatherInfo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_main_screen, parent, false)
        return WeatherHolder(itemView)
    }

    override fun getItemCount(): Int {
        return weatherList.size
    }

    override fun onBindViewHolder(holder: WeatherHolder, position: Int) {
        holder.bind(weatherList[position])
    }

    fun update(list: List<WeatherInfo>) {
        weatherList.clear()
        weatherList.addAll(list)
        notifyDataSetChanged()
    }

    class WeatherHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        fun bind(weather: WeatherInfo) {
            itemView.day_info.text = weather.dtTxt
            itemView.weather_temp.text = weather.main?.temp.toString() + " ℃"
            itemView.weather_info.text = weather.weather?.get(0)?.description
            Glide.with(itemView.context)
                .load("http://openweathermap.org/img/wn/${weather.weather?.get(0)?.icon}.png")
                .into(itemView.weather_image)
            itemView.root.setOnClickListener(
                Navigation.createNavigateOnClickListener(
                    R.id.action_mainScreen_to_detailsWeather,
                    bundleOf("weatherData" to weather))
            )
        }
    }
}
package com.skserj.weather.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.skserj.weather.interfaces.FromPresenterToMainScreenProvider
import com.skserj.weather.R
import com.skserj.weather.model.network.WeatherInfo
import com.skserj.weather.presener.MainPresenter
import kotlinx.android.synthetic.main.fragment_main_screen.*

class MainScreen : Fragment(),
    FromPresenterToMainScreenProvider {

    private lateinit var weatherAdapter: WeatherAdapter
    private lateinit var mainPresenter: MainPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val weatherType = resources.getStringArray(R.array.weather_type)
        mainPresenter = MainPresenter(this, requireContext())
        ArrayAdapter.createFromResource(requireContext(),R.array.weather_type,android.R.layout.simple_spinner_item).also {
            adapter->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mainPresenter.takeDataFromModel(weatherType[position])
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        }
        weatherAdapter = WeatherAdapter()
        recycler_view.apply {
            adapter = weatherAdapter
            layoutManager = LinearLayoutManager(this.context)
        }
    }

    override fun updateInfo(it: List<WeatherInfo>) {
        weatherAdapter.update(it)
    }

}